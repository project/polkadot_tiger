<?php

namespace Drupal\polkadot_tiger\Controller;

class PolkadotTigerPageController extends PolkadotTigerNodeController
{
  protected function nodeType()
  {
    return 'page';
  }
}

