<?php

namespace Drupal\polkadot_tiger\Controller;

class PolkadotTigerPostController extends PolkadotTigerNodeController
{
  protected function nodeType()
  {
    return 'post';
  }
}

