<?php

namespace Drupal\polkadot_tiger\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Markup;

class PolkadotTigerSettingController extends ControllerBase
{
  public function index()
  {
    $config = \Drupal::service('config.factory')
                     ->getEditable('polkadot_tiger.settings');

    $apiKey = $config->get('api_key');
    if (!$apiKey) {
      $lowNumber = rand(1, 9);
      $apiKey = 'pdt' . $lowNumber . '-' .  implode('-', str_split(substr(strtolower(md5(microtime().rand(1000, 9999))), 0, 24), 6));
      $config->set('api_key', $apiKey);
      $config->save();
    }

    return [
      '#markup' => Markup::create('Your API key: <span style="user-select: all;font-family: monospace, sans-serif;background-color:#ecf0f1;padding: .5em .75em">'.$apiKey.'</span>')
    ];
  }
}

