<?php

namespace Drupal\polkadot_tiger\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\metatag\MetatagManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class PolkadotTigerNodeController extends ControllerBase
{
  public $node;

  private $metatagManager;

  public function verify(Request $request)
  {
    if (!$this->apiKeyIsValid($request)) {
      return $this->render403();
    }

    return new JsonResponse([
      'data' => [
        'message' => 'ok'
      ]
    ]);
  }

  public function __construct(MetatagManager $metatagManager)
  {
    $this->metatagManager = $metatagManager;
  }

  public static function create(ContainerInterface $container)
  {
    return new static(
      $container->get('metatag.manager')
    );
  }

  /*
   * Retrieve all
   * https://drupal.stackexchange.com/questions/213689/get-all-nodes-of-given-type
   * */

  public function index(Request $request)
  {
    if (!$this->apiKeyIsValid($request)) {
      return $this->render403();
    }

    $nodes = $this->entityTypeManager()
                  ->getStorage('node')
                  ->loadByProperties([
                    'type'   => $this->nodeType(),
                    'status' => 1,
                  ]);

    return $this->renderNodes($nodes);
  }

  /*
   * Search
   * https://drupal.stackexchange.com/questions/213689/get-all-nodes-of-given-type
   * https://drupal.stackexchange.com/questions/228548/how-to-load-a-node-by-a-field-value
   * */
  public function search(Request $request)
  {
    if (!$this->apiKeyIsValid($request)) {
      return $this->render403();
    }

    $title = $request->query->get('title');
    if ($title) {
      $query = \Drupal::entityQuery('node')
                      ->condition('title', "%$title%", 'LIKE')
                      ->condition('type', $this->nodeType())
                      ->execute();
      $nodes = $this->entityTypeManager()
                    ->getStorage('node')
                    ->loadMultiple($query);
    } else {
      $search = [
        'type'   => $this->nodeType(),
        'status' => 1
      ];
      $nodes = $this->entityTypeManager()
                    ->getStorage('node')
                    ->loadByProperties($search);
    }

    return $this->renderNodes($nodes);
  }

  public function nodeByUrl(Request $request) {
    $pageUrl = $request->query->get('url');
    $path = null;

    if ($pageUrl == "/") {
      $config = \Drupal::config('system.site');
      $pageUrl = $config->get('page.front');
    }
    try {
      $path = \Drupal::service('path.alias_manager')->getPathByAlias($pageUrl);
    } catch (Exception $e) {
      $path = \Drupal::service('path_alias.manager')->getPathByAlias($pageUrl);
    }

    if(preg_match('/node\/(\d+)/', $path, $matches)) {
      $node = $this->findNode($matches[1]);
      return new JsonResponse($this->nodeAsJson($node));
    }

    return new JsonResponse([
      'error' => "No node found for URL: $pageUrl"
    ], 404);
  }

  public function show($nid, Request $request)
  {
    if (!$this->apiKeyIsValid($request)) {
      return $this->render403();
    }

    $node = $this->findNode($nid);

    return new JsonResponse($this->nodeAsJson($node));
  }

  // https://imalabya.co/fetch-metatags-programmatically-drupal
  public function meta($nid, Request $request)
  {
    if (!$this->apiKeyIsValid($request)) {
      return $this->render403();
    }

    $data = $this->metatagManager
      ->tagsFromEntity(
        $this->findNode($nid)
      );

    return new JsonResponse([
        'meta_title' => $data['title'],
        'meta_description' => $data['description'],
        'meta_keywords' => $data['keywords']
    ]);
  }

  public function setMeta($nid, Request $request)
  {
    if (!$this->apiKeyIsValid($request)) {
      return $this->render403();
    }

    $node = $this->findNode($nid);

    $node->set('field_meta_tags', serialize([
      'title'       => $request->request->get('meta_title'),
      'description' => $request->request->get('meta_description'),
      'keywords'    => $request->request->get('meta_keywords')
    ]));
    $node->save();

    return $this->meta($nid, $request);
  }

  protected function renderNodes($nodes)
  {
    $response = array();
    foreach ($nodes as $node) {
      $response[] = $this->nodeAsJson($node);
    }

    return new JsonResponse($response);
  }

  protected function findNode($nid)
  {
    if (!$this->node) {
      $this->node = $this->entityTypeManager()
                         ->getStorage('node')
                         ->load($nid);
    }

    return $this->node;
  }

  protected function nodeType()
  {
    return 'node';
  }

  protected function nodeAsJson($node)
  {
    return [
      'id'         => $node->id(),
      'url'        => $node->toUrl()
                           ->toString(),
      'post_title' => $node->getTitle(),
      'post_type'  => $node->getType(),
    ];
  }

  protected function apiKeyIsValid(Request $request) {
    $key = base64_decode(explode(' ', $request->headers->get('authorization'))[1]);
    if ($key) {
      $config = \Drupal::service('config.factory')
                       ->getEditable('polkadot_tiger.settings');
      $valid = explode(':', $key)[1] === $config->get('api_key');
    }

    return $key && $valid;
  }

  protected function render403() {
    return new JsonResponse([
        'error' => 'The API key is not valid'
      ], 403);
  }
}

