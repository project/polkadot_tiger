<?php

namespace Drupal\polkadot_tiger\Controller;

class PolkadotTigerArticleController extends PolkadotTigerNodeController
{
  protected function nodeType()
  {
    return 'article';
  }
}

