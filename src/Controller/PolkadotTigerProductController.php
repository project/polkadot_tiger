<?php

namespace Drupal\polkadot_tiger\Controller;

class PolkadotTigerProductController extends PolkadotTigerNodeController
{
  protected function nodeType()
  {
    return 'product';
  }
}

